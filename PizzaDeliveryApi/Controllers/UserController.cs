﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PizzaDeliveryApi.Configuration;
using PizzaDeliveryApi.Model;
using PizzaDeliveryApi.Model.Responses;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDeliveryApi.Controllers
{
    public class UserController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly JwtConfig _jwtConfig;

        public UserController(UserManager<IdentityUser> userManager, IOptionsMonitor<JwtConfig> optionsMonitor)
        {
            _userManager = userManager;
            _jwtConfig = optionsMonitor.CurrentValue;

        }

        [HttpPost]
        [Route("api/Register")]

        public async Task<IActionResult> Register([FromBody] User user) {

            if (ModelState.IsValid)
            {
                var existingUser = await _userManager.FindByEmailAsync(user.Email);

                if (existingUser != null)
                {
                    return BadRequest(new UserRegisterationDto()
                    {

                        Errors = new List<string>()
                        {
                            "Email Allready Registered"
                        },
                        Success = false
                    }) ;

                }

                var newUser = new IdentityUser() { Email = user.Email, UserName = user.Email };
                var isCreated = await _userManager.CreateAsync(newUser, user.Password);
                if (isCreated.Succeeded)
                {
                    var jwtToken = GenerateJwtToken(newUser);

                    return Ok(new UserRegisterationDto()
                    {
                        Success = true,
                        Token = jwtToken

                    });

                }
                else
                {
                    return BadRequest(new UserRegisterationDto()
                    {

                        Errors = isCreated.Errors.Select(x => x.Description).ToList(),
                        Success = false
                    });
                }

            }

            return BadRequest(new UserRegisterationDto()
            {
                Errors = new List<string>()
                {
                    "Invalid User"
                },
                Success=false
            });
        }

        [HttpPost]
        [Route("api/Login")]

        public async Task<IActionResult> UserLogin([FromBody] Login user)
        {
            if (ModelState.IsValid)
            {
                var existingUser = await _userManager.FindByEmailAsync(user.Email);

                if (existingUser == null)
                {
                    return BadRequest(new UserRegisterationDto() { 
                        Errors = new List<string>()
                        {
                            "Invalid Login Request"
                        },
                        Success = false
                    
                    });
                }

                var isCorrect = await _userManager.CheckPasswordAsync(existingUser, user.Password);

                if (!isCorrect)
                {
                    return BadRequest(new UserRegisterationDto() { 
                        Errors = new List<string>()
                        {
                            "Inavlid Login Request"
                        },
                        Success = false
                    
                    });

                }

                var jwtToken = GenerateJwtToken(existingUser);
                return Ok(new UserRegisterationDto() { 
                     
                    Success = true,
                    Token = jwtToken
                
                });


            }

            return BadRequest(new UserRegisterationDto()
            {
                Errors = new List<string>()
                {
                    "Invalid User"
                },
                Success = false
            });

        }

        private string GenerateJwtToken(IdentityUser user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("Id",user.Id),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.Sub,user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())

                }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtTokenHandler.WriteToken(token);

            return jwtToken;
        }
    }

    
}
