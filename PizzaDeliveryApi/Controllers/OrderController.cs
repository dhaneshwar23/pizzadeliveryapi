﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PizzaDeliveryApi.Data;
using PizzaDeliveryApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDeliveryApi.Controllers
{
    public class OrderController:ControllerBase
    {
        private readonly ApplicationDbContext _applicationDbCOntext;

        public OrderController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbCOntext = applicationDbContext;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        [Route("api/GetOrders")]

        public  IActionResult GetOrder()
        {
            var result =  _applicationDbCOntext.Orders.AsQueryable();
            return Ok(result);
        }

        [HttpPost]
        [Route("api/PlaceOrder")]

        public IActionResult PlaceOrder([FromBody] Order order) {

            if (order == null)
            {
                return BadRequest();
            }

            else
            {
                _applicationDbCOntext.Orders.Add(order);
                _applicationDbCOntext.SaveChanges();

                return Ok(new
                {
                    Status = 200,
                    Message = "Order Has Been Placed"

                });
            }
        }

        [HttpPut]
        [Route("api/UpdateOrder")]

        public IActionResult UpdateOrder(int Id,[FromBody] Order order)
        {
            var updateOrder = _applicationDbCOntext.Orders.FirstOrDefault(o => o.Order_Id == Id);
            if (updateOrder == null)
            {
                return BadRequest();   
            }
            else
            {
                _applicationDbCOntext.Entry<Order>(updateOrder).CurrentValues.SetValues(order);
                _applicationDbCOntext.SaveChanges();

                return Ok(new
                {
                    Status = 200,
                    Message = "Order Has been updated"
                });

            }

        }

        [HttpDelete]
        [Route("api/DeleteOrder")]

        public IActionResult DeleteOrder([FromBody] Order Order_Id)
        {
            if (Order_Id== null)
            {
                return BadRequest();
            }
            else
            {
                _applicationDbCOntext.Orders.Remove(Order_Id);
                _applicationDbCOntext.SaveChanges();

                return Ok(new
                {
                    Message = "Order Has been Deleted Succesfully"
                });
            }
        }
    }
}
