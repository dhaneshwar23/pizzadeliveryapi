﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using PizzaDeliveryApi.Model;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace PizzaDeliveryApi.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        /*public DbSet<User> user { get; set; }*/

        public DbSet<Order> Orders { get; set; }

        /*public DbSet<Login> login { get; set; }*/

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
