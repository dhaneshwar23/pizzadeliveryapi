﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaDeliveryApi.Configuration
{
    public class AuthResult
    {
        public string Token { get; set; }

        public bool Success { get; set; }

        public List<String> Errors { get; set; }
    }
}
